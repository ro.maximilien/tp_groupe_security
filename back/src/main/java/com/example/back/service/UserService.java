package com.example.back.service;

import com.example.back.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> findAll();
    Optional<User>findById(Long id);
    void save(User user);

}
