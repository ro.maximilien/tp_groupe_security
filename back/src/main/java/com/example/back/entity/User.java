package com.example.back.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private long id;

    private  String mdp;

    private  String email;
    @Column(name = "user_name")
    private  String userName;

    @OneToMany(mappedBy = "user")
    private Set<Event> events=new HashSet<>();


}
