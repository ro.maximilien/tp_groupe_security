package com.example.back.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(columnDefinition = "text")
    private String description;

    @Column(nullable = false)
    @DateTimeFormat(pattern = "dd-mm-yyyy")
    private Date date;

    @Column(nullable = false)
    private String town;
    @Column(nullable = false)
    private String country;

    @Column(nullable = false)
    private String photo;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
