package com.example.back.controller;

import com.example.back.dto.UserDto;
import com.example.back.entity.User;
import com.example.back.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;


import javax.swing.plaf.PanelUI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private  UserService userService;





    //creer un user
    @PostMapping("")
    public  ResponseEntity<UserDto>addUser(@RequestBody UserDto userDto){
        User user= new ModelMapper().map(userDto, User.class);
        if (userDto.getId()!=null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        System.out.println(user);
        userService.save(user);
        return  new ResponseEntity<>(HttpStatus.CREATED);
    }


    //un user
    @GetMapping("/{id}")
    public ResponseEntity<UserDto>getUser(@PathVariable Long id){
        Optional <User> user=userService.findById(id);
        if(user.isEmpty()){
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
        return  new ResponseEntity<>(HttpStatus.CREATED);
           }
    //trouver les uses

    @GetMapping("")
    public ResponseEntity<List<UserDto>>findAll(){
        List<UserDto>userDtoList=new ArrayList<>();
        for (User user : userService.findAll()){
            UserDto userDto=new  ModelMapper().map(user,UserDto.class);
            userDtoList.add(userDto);
        }
        return  new ResponseEntity<>(userDtoList,HttpStatus.OK);
    }
    //modifier un user
    @PutMapping("/{id}")
    public  ResponseEntity<UserDto>updateUser(@RequestBody UserDto userDto,@PathVariable Long id){
        User user= new ModelMapper().map(userDto, User.class);
        user.setId(id);
        userService.save(user);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
